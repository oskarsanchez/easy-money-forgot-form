class EasyMoneyForgotForm extends Polymer.Element {

  static get is() {
    return 'easy-money-forgot-form';
  }

  static get properties() {
    return {
      email: {
        type: String
      },
      loading:{
        type: Boolean,
        notify: true,
        value: false
      }
    };
  }
  ready (){
    super.ready();

  }

  _handleCancelarClick(evt){
    this.dispatchEvent(new CustomEvent('forgot-cancel',{bubbles:true, composed:true}));
  }

  _handleAceptarClick(evt){
    this.loading = true;
    const forgotUser = {
      email: this.email
    };
    this.$.forgotdp.host = cells.urlEasyMoneyBankAuthServices;
    this.$.forgotdp.body = forgotUser;
    this.$.forgotdp.generateRequest();
  }
  
  _handleRequestForgotSuccess(evt) {
    const detail = evt.detail;
    delete detail.headers;
    this.loading = false;
    this.dispatchEvent(new CustomEvent('forgot-ok',{bubbles:true, composed:true}));
  }
  _handleRequestForgotError(evt) {
    const detail = evt.detail;
    let msg;
    if (detail) {
      delete detail.headers;
      msg = detail.msg
    }
    this.loading = false;
    this.dispatchEvent(new CustomEvent('forgot-error', {
      bubbles: true,
      composed: true,
      detail: {
        msg: msg
      }
    }));
  }
}

customElements.define(EasyMoneyForgotForm.is, EasyMoneyForgotForm);